﻿const db = require('_helpers/db');
const Task = db.Task;
const Status = db.Status;
const i18n = require("i18n");

module.exports = {
    getAll,
    getById,
    getAllByTaskIdAndWeekNumber,
    getAllByTaskId,
    deleteAllByTaskId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Status.find().select('-hash');
}

async function getById(id) {
    return await Status.findById(id).select('-hash');
}

async function getAllByTaskIdAndWeekNumber(taskid, weekNumber) {
    const task = await Task.findById(taskid).select('-hash');
    return await Status.find({task: task, weekNumber: weekNumber});
}

async function getAllByTaskId(taskid) {
    const task = await Task.findById(taskid).select('-hash');
    return await Status.find({task: task});
}

async function deleteAllByTaskId(taskid) {
    const task = await Task.findById(taskid).select('-hash');
    await Status.remove({task: task});
}

async function create(statusParam) {
    // validate
    // if (await Status.findOne({statusName: statusParam.statusName, task: statusParam.task})) {
    //     throw i18n.__('Status Name "') + statusParam.statusName + i18n.__('" is already taken');
    // }

    const status = new Status(statusParam);
    if (status.date) {
        status.date.setHours(0);
        status.date.setMinutes(0);
        status.date.setSeconds(0);
        status.date.setMilliseconds(0);
    }
    // save status
    await status.save();
    return await status;
}

async function update(id, statusParam) {
    const status = await Status.findById(id);

    // validate
    if (!status) throw i18n.__('Status not found');
    // if (status.statusName !== statusParam.statusName && await Status.findOne({
    //     statusName: statusParam.statusName,
    //     task: statusParam.task
    // })) {
    //     throw i18n.__('Status Name "') + statusParam.statusName + i18n.__('" is already taken');
    // }

    // copy statusParam properties to status
    Object.assign(status, statusParam);
    if (status.date) {
        status.date.setHours(0);
        status.date.setMinutes(0);
        status.date.setSeconds(0);
        status.date.setMilliseconds(0);
    }
    await status.save();
}

async function _delete(id) {
    await Status.findByIdAndRemove(id);
}
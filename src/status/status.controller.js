﻿const express = require('express');
const router = express.Router();
const statusService = require('./status.service');

// routes
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.get('/task/:id/week/:weekNumber', getAllByTaskIdAndWeekNumber);
router.get('/task/:id', getAllByTaskId);
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    statusService.getAll()
        .then(status => res.json(status))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    statusService.getById(req.status.sub)
        .then(status => status ? res.json(status) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    statusService.getById(req.params.id)
        .then(status => status ? res.json(status) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByTaskIdAndWeekNumber(req, res, next) {
    statusService.getAllByTaskIdAndWeekNumber(req.params.id, req.params.weekNumber)
        .then(status => status ? res.json(status) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByTaskId(req, res, next) {
    statusService.getAllByTaskId(req.params.id)
        .then(status => status ? res.json(status) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    statusService.create(req.body)
        .then(status => status ? res.json(status) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    statusService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    statusService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}
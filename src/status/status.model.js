const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    task: {type: mongoose.Schema.Types.ObjectId, ref: 'Task', required: true},
    date: {type: Date, required: true},
    statusValue: {type: String},
    weekNumber: {type: Number, required: true},
    feedback: {type: String},
    createdDate: {type: Date, default: Date.now}
});

schema.index({date: 1, task: 1}, {unique: true});
schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Status', schema);
﻿const request = require("request");
const config = require('config');
const builder = require('xmlbuilder');
const db = require('_helpers/db');
const User = db.User;
const Board = db.Board;
const Checkout = db.Checkout;
const parseString = require('xml2js').parseString;
const moment = require('moment');

const urlSite = process.env.NODE_ENV === 'production' ? config.production.url : config.development.url;
const wsPagseguroUrl = process.env.NODE_ENV === 'production' ? config.production.wsPagseguroUrl : config.development.wsPagseguroUrl;
const credentialsPagSeguro = process.env.NODE_ENV === 'production' ? config.production.credentialsPagSeguro : config.development.credentialsPagSeguro;
const emailPagSeguro = process.env.NODE_ENV === 'production' ? config.production.emailPagSeguro : config.development.emailPagSeguro;

module.exports = {
    getAll,
    getById,
    getAllByBoardId,
    create,
    update,
    delete: _delete,
    checkout,
    savePreApprovalsRequest,
    preApprovalsRequest,
    listPreApprovals,
    cancelPayment
};

async function getAll() {
    return await Checkout.find().select('-hash');
}

async function getById(id) {
    return await Checkout.findById(id).select('-hash');
}

async function getAllByBoardId(boardid) {
    const board = await Board.findById(boardid).select('-hash');
    return await Checkout.find({board: board}).sort({date: 'asc'});
}

async function create(checkoutParams) {
    const checkoutObject = new Checkout(checkoutParams);
    return await checkoutObject.save();
}

async function update(id, checkoutParams) {
    const checkout = await Checkout.findById(id);

    // validate
    if (!checkout) throw i18n.__('Checkout not found');

    Object.assign(checkout, checkoutParams);
    await checkout.save();
}

async function _delete(id) {
    await Checkout.findByIdAndRemove(id);
}

async function checkout(userParams) {
    const url = wsPagseguroUrl + '/v2/checkout?' + credentialsPagSeguro;
    const body = builder.create('checkout')
        .ele('sender')
        .ele('name', userParams.firstName + ' ' + userParams.lastName)
        .up()
        .ele('email', userParams.username)
        .up()
        .up()
        .ele('currency', 'BRL')
        .up()
        .ele('items')
        .ele('item')
        .ele('id', '0001')
        .up()
        .ele('description', 'QUADRO DE TAREFA 3 MESES')
        .up()
        .ele('amount', '10.00')
        .up()
        .ele('quantity', '1')
        .up()
        .up()
        .up()
        .ele('redirectURL', urlSite + '/checkout/return')
        .up()
        .ele('reference', 'MON3_BRL10')
        .up()
        .ele('receiver')
        .ele('email', emailPagSeguro)
        .end({pretty: true});

    const options = {
        method: 'POST',
        url: url,
        body: body,
        headers: {
            'Content-Type': 'application/xml; charset=ISO-8859-1',
        }
    };

    return await doRequest(options);
}

async function preApprovalsRequest(checkoutParams) {
    const url = wsPagseguroUrl + '/v2/pre-approvals/request?' + credentialsPagSeguro;
    // const finalDate = new Date();
    const body = builder.create('preApprovalRequest')
        .ele('redirectURL', config.production.url + '/checkout/return')
        .up()
        // .ele('reviewURL', config.production.url + '/checkout/review')
        // .up()
        .ele('reference', checkoutParams.board.id)
        .up()
        .ele('sender')
        .ele('name', checkoutParams.user.firstName + ' ' + checkoutParams.user.lastName)
        .up()
        .ele('email', checkoutParams.user.username)
        .up()
        .up()
        .ele('preApproval')
        .ele('charge', 'auto')
        .up()
        .ele('name', 'QUADRO DE TAREFA 3 MESES')
        .up()
        .ele('details', 'Permissão para utilização de um quadro de tarefas pelo período de três meses.')
        .up()
        .ele('amountPerPayment', '10.00')
        .up()
        .ele('period', 'trimonthly')
        .up()
        // .ele('finalDate', finalDate)
        // .up()
        .ele('maxTotalAmount', '10.00')
        .end({pretty: true});

    const options = {
        method: 'POST',
        url: url,
        body: body,
        headers: {
            'Content-Type': 'application/xml; charset=ISO-8859-1',
        }
    };

    return await doRequest(options);
}

async function listPreApprovals(id) {
    const checkout = await Checkout.findById(id).select('-hash');
    const initialDate = new Date(checkout.date);
    initialDate.setTime(initialDate.getTime() - (60000));
    const initialDateString = formatDate(initialDate);
    const finalDate = new Date(checkout.date);
    finalDate.setTime(finalDate.getTime() + (60000));
    const finalDateString = formatDate(finalDate);
    const url = wsPagseguroUrl + '/pre-approvals?' + credentialsPagSeguro + '&initialDate=' + initialDateString + '&finalDate=' + finalDateString + '&preApprovalRequest=' + checkout.code;
    const options = {
        method: 'GET',
        url: url,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
        }
    };

    return await doRequest(options);
}

function formatDate(date) {
    let isoString = moment(date).format();
    isoString = isoString.replace("-02:00", ".000z");
    isoString = isoString.replace("-03:00", ".000z");
    isoString = isoString.replace("-04:00", ".000z");
    return isoString;
}

function doRequest(options) {
    // console.log('options', options);
    return new Promise(function (resolve, reject) {
        request(options, function (error, res, body) {
            if (!error && res.statusCode == 200) {
                // console.log('body', body);
                resolve(body);
            } else {
                reject(error);
                // console.log('error', error);
            }
        });
    });
}

async function savePreApprovalsRequest(checkoutParams, preApprovalsReturn) {
    const user = await User.findOne({username: checkoutParams.user.username});
    if (!user) {
        throw i18n.__('Username "') + checkoutParams.user.username + i18n.__('" not found');
    }

    const checkoutObject = new Checkout(checkoutParams);
    parseString(preApprovalsReturn, function (err, result) {
        checkoutObject.code = result.preApprovalRequest.code;
        checkoutObject.date = result.preApprovalRequest.date;
    });

    await checkoutObject.save()
}


async function cancelPayment(cancelParams) {
    const board = await Board.findById(cancelParams.boardId).select('-hash');
    const checkout = await Checkout.findOne({board: board}).sort({date: 'desc'});

    const url = wsPagseguroUrl + '/pre-approvals/' + checkout.transactionCode + '/cancel?' + credentialsPagSeguro;

    const options = {
        method: 'PUT',
        url: url,
        headers: {
            Accept: 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
        }
    };

    return await doRequest(options);
}

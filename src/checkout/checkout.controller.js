﻿const express = require('express');
const router = express.Router();
const checkoutService = require('./checkout.service');

// routes
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.get('/board/:id', getAllByBoardId);
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/checkout', checkout);
router.post('/preApprovalsRequest', preApprovalsRequest);
router.post('/cancelPayment', cancelPayment);
router.get('/listPreApprovals/:id', listPreApprovals);

module.exports = router;

function getAll(req, res, next) {
    checkoutService.getAll()
        .then(checkouts => res.json(checkouts))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    checkoutService.getById(req.checkout.sub)
        .then(checkout => checkout ? res.json(checkout) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    checkoutService.getById(req.params.id)
        .then(checkout => checkout ? res.json(checkout) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByBoardId(req, res, next) {
    checkoutService.getAllByBoardId(req.params.id)
        .then(checkouts => checkouts ? res.json(checkouts) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    checkoutService.create(req.body)
        .then(checkout => checkout ? res.json(checkout) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    checkoutService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    checkoutService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function checkout(req, res, next) {
    checkoutService.checkout(req.body)
        .then(response => res.json(response))
        .catch(err => next(err));
}

function preApprovalsRequest(req, res, next) {
    checkoutService.preApprovalsRequest(req.body)
        .then(response => {
            checkoutService.savePreApprovalsRequest(req.body, response)
                .then(response => res.json(response))
                .catch(err => next(err));
            return res.json(response);
        })
        .catch(err => next(err));
}

function listPreApprovals(req, res, next) {
    checkoutService.listPreApprovals(req.params.id)
        .then(response => res.json(response))
        .catch(err => next(err));
}

function cancelPayment(req, res, next) {
    checkoutService.cancelPayment(req.body)
        .then(response => res.json(response))
        .catch(err => next(err));
}

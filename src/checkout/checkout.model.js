const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    board: {type: mongoose.Schema.Types.ObjectId, ref: 'Board', required: true},
    code: {type: String, required: true},
    transactionCode: {type: String, required: false},
    date: {type: Date, required: true},
    status: {type: String, default: 'NOT_PAYED', required: true},
    feedback: {type: String, required: false},
    createdDate: {type: Date, default: Date.now}
});

schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Checkout', schema);

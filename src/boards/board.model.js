const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    childName: {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    childBirthDate: {type: Date, required: true},
    childGender: {type: String, required: true},
    startDate: {type: Date, default: Date.now, required: true},
    availableStartDate: {type: Date},
    availableEndDate: {type: Date},
    test: {type: Boolean, default: true, required: true},
    createdDate: {type: Date, default: Date.now, required: true},
    boardGoal: {type: Number, default: 80, required: true}
});

schema.index({childName: 1, user: 1}, {unique: true});
schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Board', schema);

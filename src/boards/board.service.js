﻿const db = require('_helpers/db');
const User = db.User;
const Board = db.Board;
const Checkout = db.Checkout;
const Status = db.Status;
const i18n = require("i18n");

const taskService = require('../tasks/task.service');
const checkoutService = require('../checkout/checkout.service');

module.exports = {
    getAll,
    getById,
    getAllByUserId,
    deleteAllByUserId,
    create,
    update,
    delete: _delete,
    getWeekPrize,
    getMonthPrize,
    getYearPrize
};

async function getAll() {
    return await Board.find().select('-hash');
}

async function getById(id) {
    const board = await Board.findById(id).select('-hash');
    return await updateBoardCheckout(board);
}

async function getAllByUserId(userid) {
    const user = await User.findById(userid).select('-hash');
    let boards = await Board.find({user: user});

    boards.forEach(async board => {
        board = await updateBoardCheckout(board);
    });

    boards = await Board.find({user: user});
    return boards;
}

async function deleteAllByUserId(userid) {
    const user = await User.findById(userid).select('-hash');
    await Board.remove({user: user});
}

async function create(boardParam) {
    // validate
    if (await Board.findOne({childName: boardParam.childName, user: boardParam.user})) {
        throw i18n.__('Child Name "') + boardParam.childName + i18n.__('" is already taken');
    }

    const board = new Board(boardParam);
    if (board.childBirthDate) {
        board.childBirthDate.setHours(0);
        board.childBirthDate.setMinutes(0);
        board.childBirthDate.setSeconds(0);
        board.childBirthDate.setMilliseconds(0);
    }

    if (board.startDate) {
        board.startDate.setHours(0);
        board.startDate.setMinutes(0);
        board.startDate.setSeconds(0);
        board.startDate.setMilliseconds(0);
    }

    if (board.test) {
        board.availableStartDate = new Date();
        board.availableStartDate.setHours(0);
        board.availableStartDate.setMinutes(0);
        board.availableStartDate.setSeconds(0);
        board.availableStartDate.setMilliseconds(0);
        board.availableEndDate = new Date();
        board.availableEndDate.setTime(board.availableEndDate.getTime() + 7 * 24 * 60 * 60 * 1000);
        board.availableEndDate.setHours(23);
        board.availableEndDate.setMinutes(59);
        board.availableEndDate.setSeconds(59);
        board.availableEndDate.setMilliseconds(0);
    }

    // save board
    return await board.save();
}

async function update(id, boardParam) {
    const board = await Board.findById(id);

    // validate
    if (!board) throw i18n.__('Board not found');
    if (board.childName !== boardParam.childName && await Board.findOne({
        childName: boardParam.childName,
        user: boardParam.user
    })) {
        throw i18n.__('Child Name "') + boardParam.boardName + i18n.__('" is already taken');
    }

    // copy boardParam properties to board
    Object.assign(board, boardParam);

    if (board.childBirthDate) {
        board.childBirthDate.setHours(0);
        board.childBirthDate.setMinutes(0);
        board.childBirthDate.setSeconds(0);
        board.childBirthDate.setMilliseconds(0);
    }

    if (board.startDate) {
        board.startDate.setHours(0);
        board.startDate.setMinutes(0);
        board.startDate.setSeconds(0);
        board.startDate.setMilliseconds(0);
    }

    await board.save();

    const tasks = await taskService.getAllByBoardId(id);
    tasks.forEach(task => {
        if (task.startDate < board.startDate) {
            task.startDate = board.startDate;
            task.save();
        }

        if (task.endDate < board.startDate) {
            task.endDate = board.startDate;
            task.save();
        }
    });

    return await updateBoardCheckout(board);
}

async function _delete(id) {
    taskService.deleteAllByBoardId(id);
    await Board.findByIdAndRemove(id);
}

async function updateBoardCheckout(board) {
    const checkouts = await checkoutService.getAllByBoardId(board.id);
    // console.log('checkouts', checkouts);
    checkouts.forEach(async checkout => {
        const preApprovals = await checkoutService.listPreApprovals(checkout.id)
            .then(async value => {
                // console.log('listPreApprovals', value);
                const preApprovalsJSON = JSON.parse(value);
                if (preApprovalsJSON && preApprovalsJSON.preApprovalList) {
                    const lastPreApproval = preApprovalsJSON.preApprovalList.pop();

                    if (lastPreApproval.status === 'ACTIVE') {
                        const availableStartDate = new Date(lastPreApproval.lastEventDate);
                        availableStartDate.setHours(0);
                        availableStartDate.setMinutes(0);
                        availableStartDate.setSeconds(0);
                        availableStartDate.setMilliseconds(0);
                        board.availableStartDate = availableStartDate;
                        const availableEndDate = new Date(availableStartDate);
                        availableEndDate.setMonth(availableStartDate.getMonth() + 3);
                        availableEndDate.setHours(23);
                        availableEndDate.setMinutes(59);
                        availableEndDate.setSeconds(59);
                        if (availableEndDate.getTime() > board.availableEndDate.getTime()) {
                            board.availableEndDate = availableEndDate;
                        }
                        board.test = false;
                        await board.save();
                    }

                    if (!lastPreApproval.status) {
                        lastPreApproval.status = 'NOT_PAYED';
                    }

                    if (lastPreApproval.code) {
                        checkout.transactionCode = lastPreApproval.code;
                    }

                    checkout.status = lastPreApproval.status;
                    await checkout.save();
                }
            })
            .catch(err => {
                if (err) {
                    // console.log(err)
                }
            });
    });

    return await Board.findById(board.id).select('-hash');
}

async function getWeekPrize(prizeParams) {
    let boardPrize = 0;
    let disabledTask = 0;
    const tasks = await taskService.getAllByBoardId(prizeParams.boardId);
    for (const task of tasks) {
        const successStatusList = await Status.find({
            task: task,
            weekNumber: prizeParams.weekNumber,
            statusValue: 'SUCCESS'
        });
        const dangerStatusList = await Status.find({
            task: task,
            weekNumber: prizeParams.weekNumber,
            statusValue: 'DANGER'
        });
        const warningStatusList = await Status.find({
            task: task,
            weekNumber: prizeParams.weekNumber,
            statusValue: 'WARNING'
        });

        const totalStatus = successStatusList.length + dangerStatusList.length + warningStatusList.length;

        if (totalStatus > 0) {
            task.prize = ((successStatusList.length * 2) + warningStatusList.length) / (totalStatus * 2);
            task.disabled = false;
        } else {
            task.prize = 0;
            task.disabled = true;
        }

        boardPrize += task.prize;
        if (task.disabled) {
            disabledTask++;
        }
    }

    return await Math.round((boardPrize / (tasks.length - disabledTask)) * 100);
}

async function getMonthPrize(prizeParams) {
    let boardPrize = 0;
    let disabledTask = 0;
    const tasks = await taskService.getAllByBoardId(prizeParams.boardId);
    for (const task of tasks) {
        const successStatusList = await Status.find({
            task: task,
            date: {
                "$gte": new Date(prizeParams.year, prizeParams.month, 1),
                "$lt": new Date(prizeParams.year, prizeParams.month, 31)
            },
            statusValue: 'SUCCESS'
        });
        const dangerStatusList = await Status.find({
            task: task,
            date: {
                "$gte": new Date(prizeParams.year, prizeParams.month, 1),
                "$lt": new Date(prizeParams.year, prizeParams.month, 31)
            },
            statusValue: 'DANGER'
        });
        const warningStatusList = await Status.find({
            task: task,
            date: {
                "$gte": new Date(prizeParams.year, prizeParams.month, 1),
                "$lt": new Date(prizeParams.year, prizeParams.month, 31)
            },
            statusValue: 'WARNING'
        });

        const totalStatus = successStatusList.length + dangerStatusList.length + warningStatusList.length;

        if (totalStatus > 0) {
            task.prize = ((successStatusList.length * 2) + warningStatusList.length) / (totalStatus * 2);
            task.disabled = false;
        } else {
            task.prize = 0;
            task.disabled = true;
        }

        boardPrize += task.prize;
        if (task.disabled) {
            disabledTask++;
        }
    }

    return await Math.round((boardPrize / (tasks.length - disabledTask)) * 100);
}

async function getYearPrize(prizeParams) {
    let boardPrize = 0;
    let disabledTask = 0;
    const tasks = await taskService.getAllByBoardId(prizeParams.boardId);
    for (const task of tasks) {
        const successStatusList = await Status.find({
            task: task,
            date: {"$gte": new Date(prizeParams.year, 0, 1), "$lt": new Date(prizeParams.year, 11, 31)},
            statusValue: 'SUCCESS'
        });
        const dangerStatusList = await Status.find({
            task: task,
            date: {"$gte": new Date(prizeParams.year, 0, 1), "$lt": new Date(prizeParams.year, 11, 31)},
            statusValue: 'DANGER'
        });
        const warningStatusList = await Status.find({
            task: task,
            date: {"$gte": new Date(prizeParams.year, 0, 1), "$lt": new Date(prizeParams.year, 11, 31)},
            statusValue: 'WARNING'
        });

        const totalStatus = successStatusList.length + dangerStatusList.length + warningStatusList.length;

        if (totalStatus > 0) {
            task.prize = ((successStatusList.length * 2) + warningStatusList.length) / (totalStatus * 2);
            task.disabled = false;
        } else {
            task.prize = 0;
            task.disabled = true;
        }

        boardPrize += task.prize;
        if (task.disabled) {
            disabledTask++;
        }
    }

    return await Math.round((boardPrize / (tasks.length - disabledTask)) * 100);
}

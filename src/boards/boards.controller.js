﻿const express = require('express');
const router = express.Router();
const boardService = require('./board.service');

// routes
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.get('/user/:id', getAllByUserId);
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/weekPrize', getWeekPrize);
router.post('/monthPrize', getMonthPrize);
router.post('/yearPrize', getYearPrize);

module.exports = router;

function getAll(req, res, next) {
    boardService.getAll()
        .then(boards => res.json(boards))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    boardService.getById(req.board.sub)
        .then(board => board ? res.json(board) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    boardService.getById(req.params.id)
        .then(board => board ? res.json(board) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByUserId(req, res, next) {
    boardService.getAllByUserId(req.params.id)
        .then(boards => boards ? res.json(boards) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    boardService.create(req.body)
        .then(board => board ? res.json(board) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    boardService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    boardService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getWeekPrize(req, res, next) {
    boardService.getWeekPrize(req.body)
        .then(weekPrize => res.json(weekPrize))
        .catch(err => next(err));
}

function getMonthPrize(req, res, next) {
    boardService.getMonthPrize(req.body)
        .then(monthPrize => res.json(monthPrize))
        .catch(err => next(err));
}

function getYearPrize(req, res, next) {
    boardService.getYearPrize(req.body)
        .then(yearPrize => res.json(yearPrize))
        .catch(err => next(err));
}

﻿const i18n = require("i18n");
const mailService = require('_helpers/mail.service');
const config = require('config');
const dateFormat = require('dateformat');

module.exports = {
    contact
};

const email = process.env.NODE_ENV === 'production' ? config.production.email : config.development.email;

async function contact(contactParam) {
    const to = email;
    const subject = i18n.__('Contact');

    const body = '<b>' + i18n.__('firstName') + ': </b>' + contactParam.firstName + '<br>' +
        '<b>' + i18n.__('lastName') + ': </b>' + contactParam.lastName + '<br>' +
        '<b>' + i18n.__('email') + ': </b>' + contactParam.email + '<br>' +
        '<b>' + i18n.__('phone') + ': </b>' + contactParam.phone + '<br>' +
        '<b>' + i18n.__('message') + ': </b>' + contactParam.message + '<br>' +
        '<b>' + i18n.__('createdDate') + ': </b>' + dateFormat(new Date(), "dd/mm/yyyy h:MM:ss");

    await mailService.send(to, subject, body, false);
}

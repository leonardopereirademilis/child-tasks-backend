﻿const express = require('express');
const router = express.Router();
const contactService = require('./contact.service');

// routes
router.post('', contact);

module.exports = router;

function contact(req, res, next) {
    contactService.contact(req.body)
        .then(value => res.json(value))
        .catch(err => next(err));
}

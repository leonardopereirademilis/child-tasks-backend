const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    taskType: {type: mongoose.Schema.Types.ObjectId, ref: 'TaskType', required: true},
    taskName: {type: String, required: true},
    board: {type: mongoose.Schema.Types.ObjectId, ref: 'Board', required: true},
    sunday: {type: Boolean, required: true, default: false},
    monday: {type: Boolean, required: true, default: false},
    tuesday: {type: Boolean, required: true, default: false},
    wednesday: {type: Boolean, required: true, default: false},
    thursday: {type: Boolean, required: true, default: false},
    friday: {type: Boolean, required: true, default: false},
    saturday: {type: Boolean, required: true, default: false},
    startDate: {type: Date, required: true, default: Date.now},
    endDate: {type: Date},
    createdDate: {type: Date, required: true, default: Date.now},
    taskGoal: {type: Number, default: 80, required: true}

});

schema.index({taskName: 1, board: 1}, {unique: true});
schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Task', schema);

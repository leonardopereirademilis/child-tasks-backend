﻿const db = require('_helpers/db');
const Board = db.Board;
const Task = db.Task;
const TaskType = db.TaskType;
const i18n = require("i18n");

const statusService = require('../status/status.service');

module.exports = {
    getAll,
    getAllTaskType,
    getById,
    getAllByBoardId,
    deleteAllByBoardId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Task.find().populate('taskType').select('-hash');
}

async function getAllTaskType() {
    return await TaskType.find().select('-hash');
}

async function getById(id) {
    return await Task.findById(id).populate('taskType').select('-hash');
}

async function getAllByBoardId(boardid) {
    const board = await Board.findById(boardid).select('-hash');
    return await Task.find({board: board}).populate('taskType');
}

async function deleteAllByBoardId(boardid) {
    const board = await Board.findById(boardid).select('-hash');
    await Task.remove({board: board});
}

async function create(taskParam) {
    // validate
    if (await Task.findOne({taskName: taskParam.taskName, board: taskParam.board})) {
        throw i18n.__('Task Name "') + taskParam.taskName + i18n.__('" is already taken');
    }

    const task = new Task(taskParam);

    if (task.startDate) {
        task.startDate.setHours(0);
        task.startDate.setMinutes(0);
        task.startDate.setSeconds(0);
        task.startDate.setMilliseconds(0);
    }

    if (task.endDate) {
        task.endDate.setHours(0);
        task.endDate.setMinutes(0);
        task.endDate.setSeconds(0);
        task.endDate.setMilliseconds(0);
    }

    // save task
    await task.save();
}

async function update(id, taskParam) {
    const task = await Task.findById(id);

    // validate
    if (!task) throw i18n.__('Task not found');
    if (task.taskName !== taskParam.taskName && await Task.findOne({
        taskName: taskParam.taskName,
        board: taskParam.board
    })) {
        throw i18n.__('Task Name "') + taskParam.taskName + i18n.__('" is already taken');
    }

    // copy taskParam properties to task
    Object.assign(task, taskParam);

    await task.save();
}

async function _delete(id) {
    statusService.deleteAllByTaskId(id);
    await Task.findByIdAndRemove(id);
}

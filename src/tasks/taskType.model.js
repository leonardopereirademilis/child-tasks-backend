const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    taskType: {type: String, required: true, unique: true},
    taskTypeName: {type: String, required: true, unique: true},
    taskIcon: {type: String, required: true},
    useTaskName: {type: Boolean, required: true, default: false},
    createdDate: {type: Date, default: Date.now}
});

schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('TaskType', schema);

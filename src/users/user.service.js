﻿const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const db = require('_helpers/db');
const User = db.User;
const i18n = require("i18n");
const mailService = require('_helpers/mail.service');
const dateFormat = require('dateformat');

const email = process.env.NODE_ENV === 'production' ? config.production.email : config.development.email;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    forgotPassword,
    resetPassword,
    authenticateSocialUser
};

async function authenticate({username, password}) {
    const user = await User.findOne({username});
    if (user && bcrypt.compareSync(password, user.hash)) {
        const {hash, ...userWithoutHash} = user.toObject();

        const token = process.env.NODE_ENV === 'production' ? jwt.sign({sub: user.id}, config.production.secret) : jwt.sign({sub: user.id}, config.development.secret);

        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    // validate
    if (await User.findOne({username: userParam.username})) {
        throw i18n.__('Username "') + userParam.username + i18n.__('" is already taken');
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    const to = userParam.username;
    const subject = i18n.__('User Created');

    const body = '<p><b>' + i18n.__('firstName') + ': </b>' + userParam.firstName + '</p>' +
        '<p><b>' + i18n.__('lastName') + ': </b>' + userParam.lastName + '</p>' +
        '<p><b>' + i18n.__('username') + ': </b>' + userParam.username + '</p>' +
        '<p><b>' + i18n.__('createdDate') + ': </b>' + dateFormat(userParam.createdDate, "dd/mm/yyyy h:MM:ss") + '</p>';

    await mailService.send(to, subject, body, true);

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw i18n.__('User not found');
    if (user.username !== userParam.username && await User.findOne({username: userParam.username})) {
        throw i18n.__('Username "') + userParam.username + i18n.__('" is already taken');
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}


async function forgotPassword(userParam) {
    const user = await User.findOne({username: userParam.username});

    if (!user) {
        throw i18n.__('Username "') + userParam.username + i18n.__('" not found');
    }

    // create the random token
    let token = null;
    const buffer = crypto.randomBytes(20);
    token = buffer.toString('hex');

    if (!token) {
        throw i18n.__('Token error');
    }

    const url = process.env.NODE_ENV === 'production' ? config.production.url : config.development.url;

    await User.findByIdAndUpdate({_id: user._id}, {
        reset_password_token: token,
        reset_password_expires: Date.now() + 86400000
    }, {upsert: true, new: true});

    const to = user.username;
    const subject = i18n.__('Password help has arrived!');

    const body = '<h3>' + i18n.__('Dear') + ' ' + user.firstName + ',</h3>' +
        '<p>' + i18n.__('You requested for a password reset for the user {{username}}, kindly use the link below to reset your password.', {username: user.username}) + '</p>' +
        '<p><a href="' + url + '/user/reset-password?token=' + token + '">' + i18n.__('Password reset') + '</a></p>' +
        '<br>' +
        '<p>' + i18n.__('Cheers!') + '</p>';

    await mailService.send(to, subject, body, true);
}

async function resetPassword(userParam) {
    const user = await User.findOne({
        reset_password_token: userParam.token,
        reset_password_expires: {
            $gt: Date.now()
        }
    });

    if (user) {
        if (userParam.newPassword === userParam.verifyPassword) {
            user.hash = bcrypt.hashSync(userParam.newPassword, 10);
            user.reset_password_token = undefined;
            user.reset_password_expires = undefined;
            if (await user.save()) {
                const to = user.username;
                const subject = i18n.__('Password Reset Confirmation');

                const body = '<h3>' + i18n.__('Dear') + ' ' + user.firstName + ',</h3>' +
                    '<p>' + i18n.__('Your password has been successful reset, you can now login with your new password.') + '</p>' +
                    '<br>' +
                    '<p>' + i18n.__('Cheers!') + '</p>';

                await mailService.send(to, subject, body, true);
            }
        } else {
            throw i18n.__('Passwords do not match')
        }
    } else {
        throw i18n.__('Password reset token is invalid or has expired')
    }
}

async function authenticateSocialUser(socialUserParam) {
    let user = await User.findOne({username: socialUserParam.email});
    if (!user) {
        socialUserParam.username = socialUserParam.email;
        user = await create(socialUserParam);
    }

    if (user) {
        const {hash, ...userWithoutHash} = user.toObject();

        const token = process.env.NODE_ENV === 'production' ? jwt.sign({sub: user.id}, config.production.secret) : jwt.sign({sub: user.id}, config.development.secret);

        return {
            ...userWithoutHash,
            token
        };
    }
}

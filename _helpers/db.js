const config = require('config');
const mongoose = require('mongoose');
mongoose.connect(process.env.NODE_ENV === 'production' ? config.production.connectionString : config.development.connectionString, {useNewUrlParser: true});
mongoose.Promise = global.Promise;

module.exports = {
    Board: require('../src/boards/board.model'),
    Status: require('../src/status/status.model'),
    Task: require('../src/tasks/task.model'),
    TaskType: require('../src/tasks/taskType.model'),
    User: require('../src/users/user.model'),
    Checkout: require('../src/checkout/checkout.model'),
};

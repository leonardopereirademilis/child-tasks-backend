const i18n = require('i18n');
const nodemailer = require('nodemailer');

module.exports = {
    send
};

const email = 'contato@quadrodetarefas.com.br';
const pass = '3dez82D?';

async function send(to, subject, body, copy) {
    let transporter = nodemailer.createTransport({
        host: 'smtpout.secureserver.net',
        port: 465,
        secure: true,
        auth: {
            user: email,
            pass: pass
        }
    });

    let bbc = '';
    if (copy) {
        bbc = email;
    }

    return await transporter.sendMail({
        from: i18n.__('Child Tasks') + ' <' + email + '>',
        to: to,
        bcc: bbc,
        subject: subject,
        html: body
    });
}
